Bootstrap AWS Debian
====================

An Ansible role to bootstrap a fresh Debian instance on an AWS EC2 host.
